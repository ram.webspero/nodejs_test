const mongoose = require("mongoose")
const Schema = mongoose.Schema;

const fromSchema = new Schema({
    name:String,
    email:String,
    phone_number:String,
    address:String,
    zip_Code:String,
    file:String
});

module.exports = mongoose.model('form',fromSchema);