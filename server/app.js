const express = require("express");
const graphqlHTTP = require("express-graphql");
const schema = require("./schema/schema");
const mongoose = require("mongoose");
const app = express();
const cors = require("cors");

var url = "mongodb://localhost:27017/nodejsTest";
mongoose.connect(url);

mongoose.connection.once("open",()=>{
    console.log("connected to database")
})

//allow cross-origin request
app.use(cors());


app.use("/graphql",graphqlHTTP({
    schema,
    graphiql:true
}));

app.listen(4000,()=>{
    console.log('now listening the request on port 4000')
})