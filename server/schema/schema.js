const graphql = require("graphql");
const _ = require("lodash");
const Form = require("../models/from");


const { GraphQLObjectType, GraphQLString, GraphQLSchema, GraphQLID, GraphQLInt, GraphQLList } = graphql;


const FormType = new GraphQLObjectType({
    name: 'Form',
    fields: () => ({
        //id:{type:GraphQLID},
        name: { type: GraphQLString },
        email: { type: GraphQLString },
        phone_number: { type: GraphQLString },
        address: { type: GraphQLString },
        zip_code: { type: GraphQLString },
        file: { type: GraphQLString },
    })
});



const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        forms: {
            type: new GraphQLList(FormType),
            resolve(parent, args) {
                //  return forms
                return Form.find({})
            }
        },
    }
})

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addForm: {
            type: FormType,
            args: {
                name: { type: GraphQLString },
                email: { type: GraphQLString },
                phone_number: { type: GraphQLString },
                address: { type: GraphQLString },
                zip_code: { type: GraphQLString },
                file: { type: GraphQLString },
            },
            resolve(parent, args) {
                let form = new Form({
                    name: args.name,
                    email: args.email,
                    phone_number: args.phone_number,
                    address: args.address,
                    zip_code: args.zip_code,
                    file:args.file
                });
                return form.save();
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})