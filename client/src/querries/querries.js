import { gql } from 'apollo-boost';

const getFormQuerry = gql`
{
    forms{
        name
        email
        phone_number
        zip_code
        address
    }
}
`


const addFormMutation = gql`
mutation($name:String!,$email:String!,$phone_number:String!,$address:String!,$zip_code:String!,$file:String!){
  addForm(name:$name,email:$email,phone_number:$phone_number,address:$address,zip_code:$zip_code,file:$file){
    name
    email
    phone_number
    address
    zip_code
    file
  }
}
`

export {addFormMutation,getFormQuerry};