import React,{Component} from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import Booklist from './Booklist.js';

const client = new ApolloClient({
  uri: 'http://localhost:4000/graphql'
})

class App extends Component {
  render() {
    return (
      <ApolloProvider client={client}>
        <div className="container-fluid" style={{ textAlign: "center" }}>
          <Booklist/>
        </div>
      </ApolloProvider>
    )
  }
}
export default App;