import React, { Component } from 'react';
import { graphql } from 'react-apollo';
import compose from 'lodash.flowright';
import { addFormMutation, getFormQuerry } from './querries/querries.js';

class Booklist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error:"",
      name: "",
      email: "",
      phone_number: "",
      address: "",
      zip_code: "",
      file: ""
    }
    console.log(this.props.getFormQuerry.data);
  }
  submitform(e) {
    if (this.state.email) {
      if (this.state.email.match("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$") ) {
        e.preventDefault();
        console.log(this.state);
        this.props.addFormMutation({
          variables: {
            name: this.state.name,
            email: this.state.email,
            phone_number: this.state.phone_number,
            address: this.state.address,
            zip_code: this.state.zip_code,
            file: this.state.file
          }
        })
        this.setState({
          name: "",
          email: "",
          phone_number: "",
          address: "",
          zip_code: "",
          file: "",
          error:""
        })
      }else{
        e.preventDefault();
        this.setState({
          error:"please enter correct email id!"
        })
      }
    }else{
      e.preventDefault();
      this.setState({
        error:"please enter email id!"
      })
    }
  }
  render() {
    return (
      <div>
        <h2>React js -- Graphql -- Apollo Client</h2>
        <form onSubmit={this.submitform.bind(this)}>
          name:<br />
          <input type="text" id="name" name="name" value={this.state.name} onChange={(e) => this.setState({ name: e.target.value })} />
          <br />
          email:<br />
          <input type="text" id="email" name="email" value={this.state.email} onChange={(e) => this.setState({ email: e.target.value })} />
          <br />
          {this.state.error}
          <br />
          Phone Number:<br />
          <input type="text" id="phone_number" name="phone_number" value={this.state.phone_number} onChange={(e) => this.setState({ phone_number: e.target.value })} />
          <br />
          Address:<br />
          <input type="text" id="address" name="address" value={this.state.address} onChange={(e) => this.setState({ address: e.target.value })} />
          <br />
          Zip Code:<br />
          <input type="text" id="zip_code" name="zip_code" value={this.state.zip_code} onChange={(e) => this.setState({ zip_code: e.target.value })} />
          <br />
          File:<br />
          <input type="file" id="file" name="file" value={this.state.file} onChange={(e) => this.setState({ file: e.target.value })} />
          <br /><br />
          <input type="submit" value="submit"/>
        </form>
        <p>If you click the "Submit" button, the form-data will be save".</p>
        {/* </Formik> */}
      </div>
    )
  }
}

export default compose(
  graphql(addFormMutation, { name: "addFormMutation" }),
  graphql(getFormQuerry, { name: "getFormQuerry" })
)(Booklist);